# servergraphqlapi
    It is a code test.
    The object is to create a server that exposes an API to manage subscriptions, create it and check their status.
    I did it using Golang, GraphQL and MySQL.

## Installations
    If you don't have Docker, Docker Compose, and Golang installed on your machine, please, follow the instructions on the links below to do that, then you will be able to start the project locally.
        * Follow steps on the Go official [download and install page](https://golang.org/doc/install)
        * Follow steps on the docker official [install page](https://docs.docker.com/get-docker/)
        * Follow steps on the docker-compose official [install page](https://docs.docker.com/compose/install/)

## Running the project
    The first thing to do is start the container with the database to run the project. To do that, run the command below.:
    
    ```shell
        docker-compose up
    ```

    The first time you run, it will take a while because docker will need to download MySQL.

    After docker is running, you will need to run the GraphQL server. To start the server, get in on folder src/ and run the code below:
    
    ```shell
        go run server.go
    ```
    Now navigate to https://localhost:8080. You can see the GraphQL playground and query the GraphQL server.

## Queries and Mutations examples:
### Queries
```graphql
      getAllMsisdn{
        id
        msisdn
        activateAt
        activateAt
        status
        type
        operator
    }

    getMsisdn(
        msisdn: String
        id: ID
    ): Msisdn!
        type Msisdn {
        id: ID!
        msisdn: String!
        activateAt: Date!
        status: StatusMsisdn!
        type: TypeMsisdn!
        operator: String!
    }
    msisdn: String
    id: ID
```

### Mutations
```graphql
    createMsisdn(
        input: NewMsisdn!
    ): Msisdn!
    type Msisdn {
        id: ID!
        msisdn: String!
        activateAt: Date!
        status: StatusMsisdn!
        type: TypeMsisdn!
        operator: String!
    }

    --------- Example -------------
    mutation{
        createMsisdn(input:{
            msisdn: "739185838",
            activateAt: "2021-01-01",
            status: ACTIVATED,
            type: CELL,
            operator: "Telia"
        }){
            id
            msisdn
            activateAt
            status
            type
            operator
        }
    }

    --------- End Example ---------


    changeActivateDateMsisdn(
        input: ActivateMsisdn!
    ): Msisdn!
    type Msisdn {
        id: ID!
        msisdn: String!
        activateAt: Date!
        status: StatusMsisdn!
        type: TypeMsisdn!
        operator: String!
    }
    input: ActivateMsisdn!


    changeStatusMsisdn(
        status: StatusMsisdn!
        id: ID!
    ): Msisdn!
    type Msisdn {
        id: ID!
        msisdn: String!
        activateAt: Date!
        status: StatusMsisdn!
        type: TypeMsisdn!
        operator: String!
    }
    status: StatusMsisdn!
    id: ID!
```

## Kafka
 It will create a new message when creating a new Msisdn using GraphQL.
 The message will show in the terminal, or you can get in on this address http://localhost:9021/
 This address will have the Control Center.
 In case of any error, you will need to create the Topic. To do it, access the link above and follow those steps:

 1. In the navigation menu, click Connect.

 2. Click the **connect-default** cluster in the Connect clusters list.

 3. Click **Add** connector to start creating a connector for **msisdncreated** data.

 4. Select the **DatagenConnector** tile.

 5. In the **Name** field, enter **datagen-msisdn** as the name of the connector.

 Enter the following configuration values:

    Key converter class: org.apache.kafka.connect.storage.StringConverter.
    kafka.topic: msisdncreated.
    max.interval: 100.
    quickstart: users.
    
 Click Next to review the connector configuration. When you’re satisfied with the settings, click Launch.

 Reviewing connector configuration in Confluent Control Center