package kafka

import (
	"context"
	"fmt"
	"strconv"

	"github.com/segmentio/kafka-go"
)

const (
	topic          = "msisdncreated"
	broker1Address = "localhost:9092"
	broker2Address = "localhost:9101"
)

func Produce(ctx context.Context, message string) {
	// init counter
	i := 0

	// writer with the broker and topic
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{broker1Address, broker2Address},
		Topic:   topic,
	})

	err := w.WriteMessages(ctx, kafka.Message{
		Key: []byte(strconv.Itoa(i)),
		// message payload
		Value: []byte(message + strconv.Itoa(i)),
	})
	if err != nil {
		// it is not the best approach, should instead log it and send an alert
		panic("could not write message" + err.Error())
	}

	// Log confirmation
	fmt.Println("Writes: ", i)
	i++
}

func Consume(ctx context.Context) {
	// init reader with broker and topic
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker1Address, broker2Address},
		Topic:   topic,
		GroupID: "msisdn-group",
	})

	msg, err := r.ReadMessage(ctx)
	if err != nil {
		panic("could not read message" + err.Error())
	}

	fmt.Println("received: ", string(msg.Value))
}
