package model

import (
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/model"
)

type Msisdn struct {
	ID         int
	Msisdn     string
	ActivateAt string
	Status     model.StatusMsisdn
	Type       model.TypeMsisdn
	Operator   string
}

type MsisdnInt interface {
	Create(input Msisdn) (payload Msisdn, err error)
}
