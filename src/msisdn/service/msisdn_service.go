package service

import (
	"context"
	"fmt"

	"github.com/shurcooL/graphql"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/model"
	msisdnModel "gitlab.com/henriquefleuryc/servergraphqlapi/msisdn/model"
	"gitlab.com/henriquefleuryc/servergraphqlapi/ptsservice"
)

var client *graphql.Client

func init() {
	client = graphql.NewClient("http://localhost:8080/query", nil)
}

func IsCancelled(msisdn string) (bool, error) {
	msisdnStc := model.Msisdn{
		Msisdn: msisdn,
	}

	rtnMsisdn, err := getMsisdn(msisdnStc)
	if err != nil {
		return false, err
	}

	if rtnMsisdn.Msisdn != "" {
		if rtnMsisdn.Status != model.StatusMsisdnCancelled {
			return false, nil
		}
	}
	return true, nil
}

func IsPending(msisdn model.Msisdn) (bool, error) {
	rtnMsisdn, err := getMsisdn(msisdn)
	if err != nil {
		return false, err
	}

	return rtnMsisdn.Status == model.StatusMsisdnPending, nil
}

func getMsisdn(msisdn model.Msisdn) (model.Msisdn, error) {
	var query struct {
		Msisdn model.Msisdn `graphql:"getMsisdn(msisdn: $msisdn, id: $id)"`
	}

	variables := map[string]interface{}{
		"id":     graphql.ID(msisdn.ID),
		"msisdn": graphql.String(msisdn.Msisdn),
	}

	if err := client.Query(context.Background(), &query, variables); err != nil {
		return model.Msisdn{}, err
	}

	return query.Msisdn, nil
}

func GetOperator(msisdn *msisdnModel.Msisdn) error {
	ptsResponse := &ptsservice.PtsResponse{
		Number: msisdn.Msisdn,
	}

	err := ptsResponse.GetOperatorByNumber()
	if err != nil {
		return fmt.Errorf("error getting the operator %v", err)
	}

	msisdn.Operator = ptsResponse.Name

	return nil

}
