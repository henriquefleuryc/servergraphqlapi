package ptsservice

import (
	"io"
	"io/ioutil"
	"net/http"
)

func httpGet(url string) (resBody []byte, status string, statusCode int, err error) {
	return httpDo("GET", url, nil)
}

func httpDo(method, url string, body io.Reader) (resBody []byte, status string, statusCode int, err error) {
	req, err := httpRequest(method, url, body)
	if err != nil {
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	resBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	status = resp.Status
	statusCode = resp.StatusCode

	return

}

func httpRequest(method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	return req, nil

}
