package ptsservice

import (
	"encoding/json"
	"fmt"
)

type ptsResponses struct {
	PtsResponse PtsResponse `json:"d"`
}

type PtsResponse struct {
	Type   string `json:"__type"`
	Name   string `json:"Name"`
	Number string `json:"Number"`
}

func (p *PtsResponse) endpoint() string {
	return "http://api.pts.se/PTSNumberService/Pts_Number_Service.svc/json/"
}

func (p *PtsResponse) createSearchByNumberEndpoint() string {
	return p.endpoint() + "SearchByNumber?Number="
}

func (p *PtsResponse) GetOperatorByNumber() error {
	url := p.createSearchByNumberEndpoint() + p.Number
	respBody, _, statusCode, err := httpGet(url)
	if err != nil {
		return err
	}

	if statusCode != 200 {
		return fmt.Errorf("failed to get %s operator", p.Number)
	}

	var respPts ptsResponses
	err = json.Unmarshal(respBody, &respPts)
	if err != nil {
		return fmt.Errorf("JSON Unmarshal fail. ERROR: %+v - Data: %s", err, string(respBody))
	}

	p.Name = respPts.PtsResponse.Name
	p.Number = respPts.PtsResponse.Number
	p.Type = respPts.PtsResponse.Type

	return nil
}
