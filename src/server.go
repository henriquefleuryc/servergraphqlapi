package main

import (
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/data"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/generated"
	"gitlab.com/henriquefleuryc/servergraphqlapi/msisdn/model"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	db := data.Conn()

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		DB: db,
	}}))

	db.AutoMigrate(&model.Msisdn{})

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))

}
