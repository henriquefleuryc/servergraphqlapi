package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/generated"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/mapper"
	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/model"
	hkfka "gitlab.com/henriquefleuryc/servergraphqlapi/kafka"
	"gitlab.com/henriquefleuryc/servergraphqlapi/msisdn/service"
)

func (r *mutationResolver) CreateMsisdn(ctx context.Context, input model.NewMsisdn) (*model.Msisdn, error) {
	ok, err := service.IsCancelled(input.Msisdn)

	if err != nil {
		return nil, fmt.Errorf("error to quering MSISDN: %s -- Error: %v", input.Msisdn, err)
	}

	if !ok {
		return nil, fmt.Errorf("MSISDN: %s already exist and it is not cancelled", input.Msisdn)
	}

	record := mapper.MapNewMsisdnInput(&input)

	if err := service.GetOperator(record); err != nil {
		return nil, err
	}

	if tx := r.DB.Create(record); tx.Error != nil {
		return nil, fmt.Errorf("error to create the new msisdn. Error: %v", tx.Error)
	}

	s := "Created a new Msisdn " + record.Msisdn

	hkfka.Produce(ctx, s)
	hkfka.Consume(ctx)

	return mapper.MapMsisdn(record), nil
}

func (r *mutationResolver) ChangeActivateDateMsisdn(ctx context.Context, input model.ActivateMsisdn) (*model.Msisdn, error) {
	validateMsisdn := model.Msisdn{
		ID: *input.ID,
	}

	ok, err := service.IsPending(validateMsisdn)
	if err != nil {
		return nil, fmt.Errorf("error to validate if MSISDN is PENDING. Error: %v", err)
	}

	if !ok {
		return nil, fmt.Errorf("the MSISDN is not PENDING")
	}

	record := mapper.MapMsisdnActivate(&input)

	if err := r.DB.Model(record).Updates(&record).Error; err != nil {
		return nil, fmt.Errorf("error to update MSISDN %d. Error: %v", record.ID, err)
	}

	return mapper.MapMsisdn(record), nil
}

func (r *mutationResolver) ChangeStatusMsisdn(ctx context.Context, status model.StatusMsisdn, id string) (*model.Msisdn, error) {
	record := model.Msisdn{
		ID:     id,
		Status: model.StatusMsisdn(status),
	}

	if err := r.DB.Model(record).Updates(record).Error; err != nil {
		return nil, fmt.Errorf("error to change the status of the msisdn %s. Error: %v", record.ID, err)
	}

	return &model.Msisdn{
		ID:         record.ID,
		Msisdn:     record.Msisdn,
		ActivateAt: record.ActivateAt,
		Status:     record.Status,
		Type:       record.Type,
		Operator:   record.Operator,
	}, nil
}

func (r *queriesResolver) GetMsisdn(ctx context.Context, msisdn *string, id *string) (*model.Msisdn, error) {
	var record model.Msisdn
	var where model.Msisdn
	if *msisdn != "" {
		where.Msisdn = *msisdn
	} else if *id != "" {
		where.ID = *id
	} else {
		return nil, fmt.Errorf("need to be provided one MSISDN or ID")
	}

	if tx := r.DB.Where(&where).Find(&record); tx.Error != nil {
		return nil, fmt.Errorf("error to get MSISDN %d. Error: %v", msisdn, tx.Error)
	}

	return &record, nil
}

func (r *queriesResolver) GetAllMsisdn(ctx context.Context) ([]*model.Msisdn, error) {
	var msisdns []*model.Msisdn
	if tx := r.DB.Find(&msisdns); tx.Error != nil {
		return nil, fmt.Errorf("error to get all MSISDN. Error: %v", tx.Error)
	}
	return msisdns, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Queries returns generated.QueriesResolver implementation.
func (r *Resolver) Queries() generated.QueriesResolver { return &queriesResolver{r} }

type mutationResolver struct{ *Resolver }
type queriesResolver struct{ *Resolver }
