package mapper

import (
	"fmt"
	"strconv"

	"gitlab.com/henriquefleuryc/servergraphqlapi/graph/model"
	msisdnModel "gitlab.com/henriquefleuryc/servergraphqlapi/msisdn/model"
)

func MapNewMsisdnInput(input *model.NewMsisdn) *msisdnModel.Msisdn {
	return &msisdnModel.Msisdn{
		Msisdn:     input.Msisdn,
		ActivateAt: input.ActivateAt,
		Status:     input.Status,
		Type:       input.Type,
	}
}

func MapMsisdn(record *msisdnModel.Msisdn) *model.Msisdn {
	idInt := strconv.Itoa(record.ID)
	return &model.Msisdn{
		ID:         idInt,
		Msisdn:     record.Msisdn,
		ActivateAt: record.ActivateAt,
		Status:     model.StatusMsisdn(record.Status),
		Type:       model.TypeMsisdn(record.Type),
		Operator:   record.Operator,
	}
}

func MapMsisdnActivate(input *model.ActivateMsisdn) *msisdnModel.Msisdn {
	idInt, err := strconv.Atoi(*input.ID)
	if err != nil {
		fmt.Print("error to parse string in int on MapMsisdnActivate")
	}
	return &msisdnModel.Msisdn{
		ID:         idInt,
		ActivateAt: input.ActivateAt,
	}
}
