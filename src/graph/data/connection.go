package data

import (
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Log *log.Logger

func Conn() (db *gorm.DB) {

	dsn := "test_user:notasecret@tcp(127.0.0.1:3306)/testdb?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		Log.Fatalln(err)
	}
	return db
}
