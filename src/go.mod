module gitlab.com/henriquefleuryc/servergraphqlapi

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0 // indirect
	github.com/segmentio/kafka-go v0.4.28
	github.com/shurcooL/graphql v0.0.0-20200928012149-18c5c3165e3a // indirect
	github.com/vektah/gqlparser/v2 v2.1.0 // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	gorm.io/driver/mysql v1.1.0 // indirect
	gorm.io/gorm v1.21.10 // indirect
)
